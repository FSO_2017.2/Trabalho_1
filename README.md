## Descrição do projeto
[Enunciado do problema](FSO20172_PROJ_PESQUISA_P1.pdf)

### Compilação e Execução
## Compilação
Para efetuar a compilação o código com o método _main_ deve estar dentro da pasta _main_. A linha de comando é:

`make MAIN_CLASS=<nome_arquivo>`

<nome_arquivo> não deve conter a extensão .c.

## Para executá-lo
`bin/<nome_arquivo>`

## Estrutura de Pacotes
* __include:__ pasta que comporta todos os cabeçalhos do código;
* __src:__ pasta que comporta o código fonte que foram definidos nos cabeçalhos;
* __main:__ pasta com os arquivos que contem o método main;
* __bin:__ pasta criada depois da execução do Makefile, mantêm os arquivos binários;
* __obj:__ pasta criada depois da execução do Makefile, mantêm os arquivos objeto de cada item compilado;
