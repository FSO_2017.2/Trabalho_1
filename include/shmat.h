#ifndef SHMAT_H
#define SHMAT_H

#include <sys/ipc.h>

key_t KEY;
int SIZE;
char *PATH;
int FLAG;

int shmid;
char *memory;

void initialize();
void create_shmid(int shm_flags);
void couple();
void register_msg(char *msg);
void destroy();

void errors(char *msg, int comp_val);

#endif
