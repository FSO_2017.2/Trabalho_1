#ifndef PIPES_H
#define PIPES_H
#include <string.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <unistd.h>

// #define QUEUENAME "queue"

// typedef struct Queue{
  // typedef struct pipes Pipes;

  int queue_open(mode_t mode, char *QUEUENAME);
  void send_queue(char *msg);
  void create(char *QUEUENAME);
  int read_queue(char *buffer);
  void end_comunication();
// } Queue;

#endif
