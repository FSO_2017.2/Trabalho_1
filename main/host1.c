#include "pipes.h"
#include "shmat.h"
#include <unistd.h>
#include <sys/file.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <stdio.h>
#define VALUE_LENGHT 100

int main()
{
  char value[VALUE_LENGHT];
  char *QUEUENAME = "f1";
  pid_t pid = fork();

  if(pid)
  {
    printf("Pai: %d\n", getpid());
    queue_open(O_WRONLY, QUEUENAME);
    while(1)
    {
      printf("Digite a mensagem(pid: %d): ", getpid());
      scanf("%s", value);
      send_queue(value);
    }
    end_comunication();

  } else {
    printf("Filho: %d\n", getpid());

    create(QUEUENAME);

    if (queue_open(O_RDONLY, QUEUENAME) < 0)
    {
      printf("Erro na abertura da fila\n");
      exit(1);
    }

    initialize();
    create_shmid(IPC_CREAT|IPC_EXCL|SHM_R|SHM_W);
    couple();
    while (read_queue(value))
    {
      while(strcmp("", memory) != 0 && strcmp("\n", value) != 0);
      printf("Filho enviando\n");
      register_msg(value);
      if (strcmp(memory, "quit") == 0) {
        end_comunication();
        exit(0);
      }
    }
  }
  return 0;
}
