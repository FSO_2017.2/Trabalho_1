#include "pipes.h"
#include "shmat.h"
#include <unistd.h>
#include <sys/file.h>
#include <stdlib.h>
#include <stdio.h>
#define VALUE_LENGHT 100

int main()
{
  char value[VALUE_LENGHT];
  pid_t pid = fork();
  char *QUEUENAME = "f2";

  if(pid)
  {
    printf("Pai: %d\n", getpid());

    // sleep(5);
    create(QUEUENAME);

    if (queue_open(O_RDONLY, QUEUENAME) < 0)
    {
      printf("Erro na abertura da fila\n");
      exit(1);
    }

    while (read_queue(value))
    {
      printf("\nresultado do pid %d: %s\n", getpid(), value);
    }
    end_comunication();


  } else {
    printf("Filho: %d\n", getpid());
    queue_open(O_WRONLY, QUEUENAME);
    char *actual = "";
    initialize();
    create_shmid(0);
    couple();
    while(1)
    {
      if(strcmp(actual, memory) != 0)
      {
        char x[100];
        strcpy(x, memory);
        // printf("Filho enviando\n");
        printf("Filho recebeu: %s\n", x);
        // scanf("%s", value);

        send_queue(x);
        if (strcmp("quit", x) == 0) {
          destroy();
          exit(0);
        }
        sleep(5);
        register_msg("");
      }
    }
    end_comunication();
  }
  return 0;
}
