#include "pipes.h"
#include <stdio.h>

int queue;

int queue_open(mode_t mode, char *QUEUENAME){

  do {
    queue = open(QUEUENAME, mode);
  } while(queue == -1);

  return queue;
}

void send_queue(char *msg){
  write(queue, msg, strlen(msg) + 1);
}

void create(char *QUEUENAME){
  mkfifo(QUEUENAME, 0660);
}

int read_queue(char *buffer){
  int result = read(queue, buffer, sizeof(buffer));

  return result;
}

void end_comunication(){
  close(queue);
}
