#include "shmat.h"
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void initialize()
{
  KEY = 123;
  SIZE = 1024;
  PATH = "fso";
  FLAG = 0 ;
}

void create_shmid(int shm_flags)
{
  shmid = shmget(ftok(PATH, KEY), SIZE, shm_flags);
  errors("Erro no shmget", shmid);
}

void couple()
{
  memory = shmat(shmid, 0, FLAG);
  errors("acoplamento impossivel(msg)", *((int *)memory));
}

void register_msg(char *msg)
{
  strcpy(memory, msg);
}

void destroy()
{
  errors("Erro shmctl()", shmctl(shmid, IPC_RMID, NULL));
}

void errors(char *msg, int comp_val)
{
  if (comp_val == -1)
  {
    printf("%s\n", msg) ;
		exit(1);
	}
}
